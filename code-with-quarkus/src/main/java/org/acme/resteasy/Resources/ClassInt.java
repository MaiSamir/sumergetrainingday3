package org.acme.resteasy.Resources;
import org.acme.resteasy.Models.Class;



import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;


@Path("/classes")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public interface ClassInt {

    @GET
    Response getClasses(
             @QueryParam("start") int start
            ,@QueryParam("size") int size
            ,@QueryParam("name") String name);

    @Path("/{classId}")
    @GET
    Response getClassByID(@PathParam("classId") Long id);

    @POST
    Response addClass(Class c,@Context UriInfo uriInfo);

    @Path("/{classId}")
    @PUT
    Response updateClass(@PathParam("classId") Long id, Class c);

    @Path("/{classId}")
    @DELETE
    Response deleteClass(@PathParam("classId") Long id);






}
