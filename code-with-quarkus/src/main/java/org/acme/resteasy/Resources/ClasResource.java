package org.acme.resteasy.Resources;

import org.acme.resteasy.Models.Class;

import org.acme.resteasy.Services.ClassService;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.net.URI;
import java.util.List;


public class ClasResource implements  ClassInt{
    ClassService classService=new ClassService();

    @Override
    public Response getClasses(int start, int size, String name) {
        //Paginated Classes
        if(start>=0 && size>0) {
            List<Class> classes= classService.getAllClasses();
            //start out of bound
            if(start>classes.size()){
                return(Response.status(404).entity("start greater than students count").build());
            }
            //size out of bound - return from start till end
            if(start+size>classes.size()){
                return(Response.ok(classService.getClassesPaginated(start,classes.size()-start)).build());
            }
            //return requested page
            return(Response.ok(classService.getClassesPaginated(start, size)).build());

        }
        //Filter by Name
        if(name!=null) {
            return (Response.ok(classService.getClassesByName(name)).build());
        }
        //All Classes Unfiltered
        return(Response.ok(classService.getAllClasses()).build());
    }

    @Override
    public Response getClassByID(Long id) {
        Class c=classService.getClassByID(id);

        //Class return
        return Response.ok(c).build();
    }

    @Override
    public Response addClass(Class c, UriInfo uriInfo) {

        Class newClass= classService.addClass(c);
        URI uri= uriInfo.getAbsolutePathBuilder().path(String.valueOf(newClass.getId())).build();

        return Response.created(uri).entity(newClass).build();
    }

    @Override
    public Response updateClass(Long id, Class c) {

        c.setId(id);
        return (Response.ok(classService.updateClass(c)).build());
    }

    @Override
    public Response deleteClass(Long id) {

        //Class deleted successfully
        return Response.ok(classService.deleteClass(id)).build();
    }



}
