package org.acme.resteasy.database;

import org.acme.resteasy.Models.Class;
import org.acme.resteasy.Models.Student;

import java.util.HashMap;
import java.util.Map;

public class DatabaseClass {


    private static Map<Long,Student> students= new HashMap<Long,Student>();
    private static Map<Long, Class> classes= new HashMap<Long,Class>();

    public DatabaseClass() {
        students.put(1L, new Student(1L, "John", "CS", 2019));
        students.put(2L, new Student(2L, "Doe", "CS", 2019));
        students.put(3L, new Student(3L, "Nancy", "CS", 2019));
        students.put(4L, new Student(4L, "Sam", "CS", 2019));

        students.put(5L, new Student(5L, "Prince", "Management", 2020));
        students.put(6L, new Student(6L, "Sarah", "Management", 2020));
        students.put(7L, new Student(7L, "Noah", "Management", 2020));
        students.put(8L, new Student(8L, "Max", "Management", 2020));
        students.put(9L, new Student(9L, "Farid", "Management", 2020));

        Map<Long,Student> class1= new HashMap<Long,Student>();
        class1.put(1L,students.get(1L));
        class1.put(2L,students.get(2L));
        class1.put(3L,students.get(3L));
        class1.put(4L,students.get(4L));

        Map<Long,Student> class2= new HashMap<Long,Student>();
        class2.put(5L,students.get(5L));
        class2.put(6L,students.get(6L));
        class2.put(7L,students.get(7L));
        class2.put(8L,students.get(8L));
        class2.put(9L,students.get(9L));



        classes.put(1L,new Class(1L,"class 1", class1) );
        classes.put(2L,new Class(2L,"class 2",class2));


    }

    public static Map<Long, Student> getStudents() {
        return students;
    }

    public static Map<Long, Class> getClasses() {
        return classes;
    }




}
