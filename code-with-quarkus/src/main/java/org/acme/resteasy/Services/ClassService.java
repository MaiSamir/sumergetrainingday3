package org.acme.resteasy.Services;


import java.util.ArrayList;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.acme.resteasy.Models.Class;
import org.acme.resteasy.Models.Student;
import org.acme.resteasy.database.DatabaseClass;
import org.acme.resteasy.exceptions.DataNotFound;
import org.acme.resteasy.exceptions.WrongFormat;


public class ClassService{
    private static Map<Long, Class> classes= DatabaseClass.getClasses();
    private static Map<Long,Student> students= DatabaseClass.getStudents();



    public ClassService(){

    }
    public List<Class> getAllClasses(){

        return new ArrayList<>(classes.values());
    }



    public List<Class> getClassesByName(String name){

        return classes.values().stream()
                .filter(aClass -> aClass.getName().equals(name))
                .collect(Collectors.toList());
    }

    public List<Class> getClassesPaginated(int start, int size){
        return new ArrayList<>(classes.values()).subList(start,start+size);
    }

    public Class getClassByID(Long id){
        if(!classes.containsKey(id)){
            throw new DataNotFound("Class with id "+id+" not found");
        }
        return classes.get(id);
    }

    public Class addClass(Class c){
        long newID= (classes.size() + 1);
        while(classes.containsKey(newID)) {
            newID+=1;}
        if(c.getName()==null){
            throw new WrongFormat("Class must have name field");
        }
        if(c.getStudents()==null||c.getStudents().isEmpty()){
            throw new WrongFormat("Class must have students field and can not be empty");
        }
        for (long id:c.getStudents().keySet()) {
            if(!students.containsKey(id)){
                throw new DataNotFound("Student with id "+id+" not found");
            }
        }

        c.setId(newID);
        classes.put(c.getId(),c);
        return c;

    }
    public Class updateClass(Class c){
        //check students exist
        if(!classes.containsKey(c.getId())){
            throw new DataNotFound("Class with id "+c.getId()+" not found");
        }
        if(c.getName()==null){
            throw new WrongFormat("Class must have name field");
        }
        if(c.getStudents()==null||c.getStudents().isEmpty()){
            throw new WrongFormat("Class must have students field and can not be empty");
        }
        for (long id:c.getStudents().keySet()) {
            if(!students.containsKey(id)){
                throw new DataNotFound("Student with id "+id+" not found");
            }
        }
        classes.put(c.getId(),c);
        return c;


    }

    public Class deleteClass(Long id){
        if(!classes.containsKey(id)){
            throw new DataNotFound("Class with id "+id+" not found");
        }
        return classes.remove(id);
    }




}