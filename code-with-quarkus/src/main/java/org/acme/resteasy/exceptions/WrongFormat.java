package org.acme.resteasy.exceptions;

public class WrongFormat extends RuntimeException {
    public WrongFormat(String message) {
        super(message);
    }
}
