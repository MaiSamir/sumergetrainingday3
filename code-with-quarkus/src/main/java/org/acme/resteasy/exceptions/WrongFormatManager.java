package org.acme.resteasy.exceptions;

import org.acme.resteasy.Models.ErrorMessage;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class WrongFormatManager implements ExceptionMapper<WrongFormat> {


    @Override
    public Response toResponse(WrongFormat exception) {
        ErrorMessage errorMessage=new ErrorMessage(exception.getMessage(),400,"");
        return Response.status(400).entity(errorMessage).build();
    }
}
