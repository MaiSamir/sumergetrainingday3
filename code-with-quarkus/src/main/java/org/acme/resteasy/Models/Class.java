package org.acme.resteasy.Models;
import java.util.Map;

public class Class{

    private long id;
    private String name;



    private Map<Long,Student> students;



    public Class(){

    }
    public Class(Long id, String name, Map <Long,Student> students){
        this.id=id;
        this.name=name;
        this.students=students;
    }



    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public Map<Long, Student> getStudents() {
        return students;
    }

    public void setStudents(Map<Long, Student> students) {
        this.students = students;
    }
}